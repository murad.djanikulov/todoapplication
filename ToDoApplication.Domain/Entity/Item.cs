﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoApplication.Domain.Entity
{
    public class Item
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public int ToDoId { get; set; }

        public ItemStatus Status { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime DueDate { get; set; }

    }
    public enum ItemStatus
    {
        Completed,
        InProgress,
        NotStarted
    }

}
