﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public class ToDoRepository : IRepository<ToDo>
    {
        private readonly ToDoDbContext context;

        public ToDoRepository(ToDoDbContext ctx)
        {
            context = ctx;
        }
        public ToDo Create(ToDo toDo)
        {
            toDo.IsHidden = false;
            context.ToDos.Add(toDo);
            context.SaveChanges();
            return toDo;
        }

        public bool Delete(int id)
        {
            var toDo = context.ToDos.Find(id);
            if (toDo != null)
            {
                context.ToDos.Remove(toDo);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public ToDo Get(int id)
        {
            return context.ToDos.Find(id);
        }

        public IEnumerable<ToDo> GetAll()
        {
            return context.ToDos.Where(toDo => !toDo.IsHidden).OrderBy(toDo => toDo.Id).ToList();
        }

        public ToDo Update(ToDo toDo)
        {
            var updatedToDo = context.ToDos.FirstOrDefault(t => t.Equals(toDo));
            if (updatedToDo != null)
            {
                updatedToDo.Name = toDo.Name;
                context.Update(updatedToDo);
                context.SaveChanges();
                return toDo;
            }
            return null;
        }
    }
}
