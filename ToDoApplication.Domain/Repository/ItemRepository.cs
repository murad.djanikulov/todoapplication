﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoApplication.Domain.Entity;

namespace ToDoApplication.Domain.Repository
{
    public class ItemRepository : IRepository<Item>
    {
        private readonly ToDoDbContext context;

        public ItemRepository(ToDoDbContext ctx)
        {
            context = ctx;
        }

        public Item Create(Item item)
        {
            ToDo toDo = context.ToDos.FirstOrDefault(t => t.Id == item.ToDoId);
            if (toDo != null)
            {
                item.CreationDate = DateTime.Now;
                context.Items.Add(item);
                context.SaveChanges();
                return item;
            }
            return null;
        }

        public bool Delete(int id)
        {
            var item = context.Items.Find(id);
            if (item != null)
            {
                context.Items.Remove(item);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public Item Get(int id)
        {
            return context.Items.Find(id);
        }

        public IEnumerable<Item> GetAll()
        {
            return context.Items.ToList();
        }

        public Item Update(Item item)
        {
            var updatedItem = context.Items.FirstOrDefault(t => t.Equals(item));
            if (updatedItem != null)
            {
                updatedItem.Description = item.Description;
                updatedItem.Status = item.Status;
                updatedItem.DueDate = item.DueDate;
                context.Update(updatedItem);
                context.SaveChanges();
                return item;
            }
            return null;
        }
    }
}
