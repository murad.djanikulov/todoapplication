using NUnit.Framework;

using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoApplication.Domain.Entity;
using ToDoApplication.Domain.Repository;

namespace ToDoApplication.Tests
{
    [TestFixture]
    public class ItemRepositoryTests
    {
        private ToDoDbContext _context;
        private ItemRepository _itemRepository;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<ToDoDbContext>()
                .UseSqlServer(@"Server=(localdb)\mssqllocaldb;database=ToDoDB;trusted_connection=true;")
                .Options;
            _context = new ToDoDbContext(options);

            _itemRepository = new ItemRepository(_context);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void Create_ValidItem_ShouldAddItemToContext()
        {
            var toDo = new ToDo { Name = "Test ToDo" };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var item = new Item { ToDoId = toDo.Id };

            var result = _itemRepository.Create(item);

            Assert.That(result, Is.Not.Null);
            Assert.That(_context.Items.Contains(item), Is.True);
        }

        [Test]
        public void Create_InvalidToDoId_ShouldNotAddItemToContext()
        {
            var invalidToDoId = 9999;
            var item = new Item { ToDoId = invalidToDoId };

            var result = _itemRepository.Create(item);

            Assert.That(result, Is.Null);
            Assert.That(_context.Items.Contains(item), Is.False);
        }

        [Test]
        public void Delete_ExistingId_ShouldRemoveItemFromContext()
        {

            ToDo toDo = new ToDo { IsHidden = false };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var existingItem = new Item();

            existingItem = new Item { ToDoId = toDo.Id };
            _context.Items.Add(existingItem);
            _context.SaveChanges();

            var result = _itemRepository.Delete(existingItem.Id);

            Assert.That(result, Is.True);
            Assert.That(_context.Items.Contains(existingItem), Is.False);
        }

        [Test]
        public void Delete_NonExistingId_ShouldReturnFalse()
        {
            var nonExistingId = 9999;

            var result = _itemRepository.Delete(nonExistingId);

            Assert.IsFalse(result);
        }

        [Test]
        public void Get_ExistingId_ShouldReturnItem()
        {

            ToDo toDo = new ToDo { IsHidden = false };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var existingItem = new Item { ToDoId = toDo.Id };
            _context.Items.Add(existingItem);
            _context.SaveChanges();

            var result = _itemRepository.Get(existingItem.Id);

            Assert.That(result, Is.EqualTo(existingItem));
        }

        [Test]
        public void Get_NonExistingId_ShouldReturnNull()
        {
            var nonExistingId = 9999;

            var result = _itemRepository.Get(nonExistingId);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetAll_ShouldReturnAllItems()
        {
            _context.ToDos.RemoveRange(_context.ToDos.ToList());
            _context.Items.RemoveRange(_context.Items.ToList());
            ToDo toDo = new ToDo { IsHidden = false };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var items = new List<Item>
        {
            new Item { ToDoId = toDo.Id },
            new Item { ToDoId = toDo.Id },
            new Item { ToDoId = toDo.Id }
        };
            _context.Items.AddRange(items);
            _context.SaveChanges();

            var result = _itemRepository.GetAll();

            Assert.That(result.Count(), Is.EqualTo(items.Count));
            CollectionAssert.AreEquivalent(items, result);
        }

        [Test]
        public void Update_ExistingItem_ShouldUpdateItemAndSaveChanges()
        {

            ToDo toDo = new ToDo { IsHidden = false };
            _context.ToDos.Add(toDo);
            _context.SaveChanges();

            var existingItem = new Item { Description = "ExistingItem", Status = ItemStatus.InProgress, ToDoId = toDo.Id };
            _context.Items.Add(existingItem);
            _context.SaveChanges();

            var updatedItem = new Item { Id = existingItem.Id, Description = "UpdatedItem", Status = ItemStatus.Completed, ToDoId = toDo.Id };

            var result = _itemRepository.Update(updatedItem);

            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.EqualTo(updatedItem));

        }

        [Test]
        public void Update_NonExistingItem_ShouldReturnNull()
        {

            var nonExistingItem = new Item { Id = 9999, Description = "NonExisting Item", Status = ItemStatus.InProgress };

            var result = _itemRepository.Update(nonExistingItem);

            Assert.That(result, Is.Null);
        }
    }
}

