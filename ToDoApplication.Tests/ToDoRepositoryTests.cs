﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using ToDoApplication.Domain.Entity;
using ToDoApplication.Domain.Repository;

namespace ToDoApplication.Tests
{


    [TestFixture]
    public class ToDoRepositoryTests
    {
        private ToDoDbContext _context;
        private ToDoRepository _toDoRepository;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<ToDoDbContext>()
            .UseSqlServer(@"Server=(localdb)\mssqllocaldb;database=ToDoDB;trusted_connection=true;")
            .Options;
            _context = new ToDoDbContext(options);

            _toDoRepository = new ToDoRepository(_context);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void CreateToDo_ShouldAddNewToDoToContext()
        {
            var newToDo = new ToDo { Name = "NewToDo" };

            var result = _toDoRepository.Create(newToDo);

            Assert.That(result, Is.EqualTo(newToDo));
            Assert.Contains(newToDo, _context.ToDos.ToList());
        }

        [Test]
        public void DeleteToDo_ExistingId_ShouldRemoveToDoFromContext()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo" };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();
            var existingId = existingToDo.Id;

            var result = _toDoRepository.Delete(existingId);

            Assert.That(result, Is.True);
            Assert.That(_context.ToDos.Contains(existingToDo), Is.False);
        }

        [Test]
        public void DeleteToDo_NonExistingId_ShouldReturnFalse()
        {
            var nonExistingId = 999;

            var result = _toDoRepository.Delete(nonExistingId);

            Assert.That(result, Is.False);
        }

        [Test]
        public void GetToDo_ExistingId_ShouldReturnToDo()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo" };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();
            var existingId = existingToDo.Id;

            var result = _toDoRepository.Get(existingId);

            Assert.That(result, Is.EqualTo(existingToDo));
        }

        [Test]
        public void GetToDo_NonExistingId_ShouldReturnNull()
        {
            var nonExistingId = 9999;

            var result = _toDoRepository.Get(nonExistingId);

            Assert.That(result, Is.Null);
        }

        [Test]
        public void GetAll_ShouldReturnVisibleToDosOrderedById()
        {
            _context.ToDos.RemoveRange(_context.ToDos.ToList());

            var visibleToDos = new List<ToDo>
        {
            new ToDo { Name = "ToDo1", IsHidden = false },
            new ToDo { Name = "ToDo2", IsHidden = false },
            new ToDo { Name = "ToDo3", IsHidden = false },
        };
            var hiddenToDo = new ToDo { Name = "HiddenToDo", IsHidden = true };
            _context.ToDos.AddRange(visibleToDos);
            _context.ToDos.Add(hiddenToDo);
            _context.SaveChanges();

            var result = _toDoRepository.GetAll();

            Assert.That(result.Count(), Is.EqualTo(visibleToDos.Count));
            Assert.That(result.All(toDo => !toDo.IsHidden), Is.True);
            Assert.That(result.OrderBy(toDo => toDo.Id).SequenceEqual(visibleToDos), Is.True);
        }

        [Test]
        public void Update_ExistingToDo_ShouldUpdateToDoAndSaveChanges()
        {
            var existingToDo = new ToDo { Name = "ExistingToDo", IsHidden = false };
            _context.ToDos.Add(existingToDo);
            _context.SaveChanges();

            var updatedToDo = new ToDo { Id = existingToDo.Id, Name = "UpdatedToDo", IsHidden = false };

            var result = _toDoRepository.Update(updatedToDo);

            Assert.That(result, Is.EqualTo(updatedToDo));

            var updatedEntity = _context.ToDos.First(t => t.Id == updatedToDo.Id);
            Assert.That(updatedEntity.Name, Is.EqualTo(updatedToDo.Name));
            Assert.That(updatedEntity.IsHidden, Is.False);
        }

        [Test]
        public void Update_NonExistingToDo_ShouldReturnNull()
        {
            var nonExistingToDo = new ToDo { Id = 9999, Name = "NonExistingToDo", IsHidden = false };

            var result = _toDoRepository.Update(nonExistingToDo);

            Assert.That(result, Is.Null);
        }

    }

}
